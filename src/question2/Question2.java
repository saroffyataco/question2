/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package question2;

import java.util.Scanner;
/**
 *
 * @author syataco
 */
public class Question2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Escriba una frase: ");
        String phrase = scanner.nextLine();
        
        if (isPalindrome(phrase)) {
            System.out.println("Es palíndromo");
        } else {
            System.out.println("No es palíndromo");
        }
            
    }
    
    static boolean isPalindrome(String phrase)
    {
        String string = phrase.replaceAll(" ","").toLowerCase();
        int i=0;
        while (i < string.length() / 2) {
            if (string.charAt(i) != string.charAt(string.length() -1 -i)) {
                return false;
            }
            i++;
        }
        return true;
    }
}
